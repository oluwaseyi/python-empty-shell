#!/usr/bin/env bash

virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
python3 manage.py db init
python3 manage.py db migrate
python3 manage.py db upgraade
python3 manage.py deplou
