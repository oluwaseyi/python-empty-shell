#!/usr/bin/env bash

source venv/bin/activate
echo "Starting server with low i/o priority"
#ionice keep the process at low enough disk and CPU priority
nohup python3 mileage_estimator_manage.py runserver >/dev/null 2>&1 &
